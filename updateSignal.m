function z = updateSignal (y)
%CALLSIGNAL Summary of this function goes here
%   Detailed explanation goe
    switch y.type
        case 'DC'
            z = DC(y.amplitude,y.xAxis);
        case 'RAMP'
            z = ramp(y.slope,y.intercept,y.xAxis);
        case 'EXPONENTIAL'
            z = exponential(y.amplitude,y.exponent,y.xAxis);
        case 'SINUSOIDAL'
            z =  sinusoidal(y.amplitude,y.frequency,y.phase,y.xAxis);
        case 'POLYNOMIAL'
            z = polynomial(y.amplitude,y.power,y.poly,y.xAxis);
        otherwise
            fprintf("ERROR: TYPE %s IS UNDEFINED",y.type);
    end
end

