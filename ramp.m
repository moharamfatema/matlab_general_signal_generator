function y = ramp(slope,intercept,x)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
y.type = 'RAMP';
y.slope = slope;
y.intercept = intercept;
y.xAxis = x;
y.signal = y.slope.*y.xAxis + y.intercept;
end

