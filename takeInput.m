function [data,n] = takeInput()
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    data.frequency_sampling= input('Enter the sampling frequency:\n');
    data.start_time=input('Enter start time:\n');
    data.end_time= input('Enter end time:\n');
    n = input('Enter the number of break points:\n');
    if n == 0 
        data.break_points =[];
    else
        for i = 1:n
            fprintf('Enter break point no. %d:\n',i);
            data.break_points(i) = input('');  
        end
    end
end

