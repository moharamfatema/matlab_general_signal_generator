function y = polynomial(amp,power,p,x)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

y.type = 'POLYNOMIAL';
y.amplitude = amp;
y.power =  power;
y.xAxis = x;
y.poly = p;

y.signal = y.amplitude.*polyval(p,y.xAxis);
end

%plot(linspace(-8,8,16),polynomial(1,3,[1,0,0,7],linspace(-8,8,16)))