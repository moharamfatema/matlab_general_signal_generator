function y = sinusoidal(amp,freq,theta,x)
%SINUSOIDAL Summary of this function goes here
%   Detailed explanation goes here
y.type = 'SINUSOIDAL';
y.amplitude = amp;
y.frequency = freq;
y.phase = theta;
y.xAxis = x;
y.signal = (y.amplitude).*sin(2*pi*(y.frequency).*(y.xAxis) + (y.phase));
end

