function y = DC(amplitude,x)
    y.type = 'DC';
    y.amplitude = amplitude;
    y.xAxis = x;
    y.signal = y.amplitude.*ones(1,length(y.xAxis));
end

