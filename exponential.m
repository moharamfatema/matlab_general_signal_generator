function y = exponential(amp,exponent,x)
%EXPONENTIAL Summary of this function goes here
%   Detailed explanation goes here
y.type = 'EXPONENTIAL';
y.amplitude = amp;
y.exponent = exponent;
y.xAxis  = x;
y.signal = y.amplitude.*(y.exponent.^y.xAxis);
end

