close all;
clc;
clear;
syms y n;
repeat = y;

while repeat == y
    [data,nbp] = takeInput();
    disp(data)
    repeat = input('Would you like to re-enter the data?(y/n):\n');
end
clear y n repeat;

T = data.end_time - data.start_time;
t = linspace(data.start_time,data.end_time,data.frequency_sampling.*T);
% data.t_sub = [];
clear T;

if nbp >0
    for i = 1:1+length(data.break_points)
        if i == 1
            data.t_sub{i} = linspace(data.start_time,data.break_points(i),data.frequency_sampling .* (data.break_points(i) - data.start_time));
        else
            if i == 1+length(data.break_points)
            data.t_sub{i} = linspace(data.break_points(end),data.end_time,data.frequency_sampling.*(data.end_time - data.break_points(end)));

            else
            data.t_sub{i} = linspace(data.break_points(i-1),data.break_points(i),data.frequency_sampling.*(data.break_points(i) - data.break_points(i-1))); 
            end
        end
    end
    disp(data.t_sub)
    
else
    data.t_sub{1} = t;
end

data.signals = {};
i = 1;
while i <= 1+length(data.break_points)
    fprintf('For the time interval no. %d:\nEnter the type of the signal. choose from the following \nDC - RAMP - POLYNOMIAL - EXPONENTIAL - SINUSOIDAL\n note: ALL CAPS\n',i);
    y.type = input('','s');
    switch y.type
        case 'DC'
            y.amplitude = input('Enter the amplitude of the DC signal:\n');

        case 'RAMP'
            y.slope = input('Enter the slope of the RAMP signal:\n');
            y.intercept = input('Enter the intercept of the RAMP signal:\n');

        case 'EXPONENTIAL'
            y.amplitude = input('Enter the amplitude of the EXPONENTIAL signal:\n');
            y.exponent = input('Enter the exponent of the EXPONENTIAL signal:\n');

        case 'SINUSOIDAL'
            y.amplitude = input('Enter the amplitude of the SINUSOIDAL signal:\n');
            y.frequency = input('Enter the frequency of the SINUSOIDAL signal:\n');
            y.phase = input('Enter the phase of the SINUSOIDAL signal:\n');

        case 'POLYNOMIAL'
            y.amplitude = input('Enter the amplitude of the POLYNOMIAL signal:\n');
            y.power = input('Enter the power of the POLYNOMIAL signal:\n');
            y.poly = input('Enter the coeffecients of the POLYNOMIAL signal in form of a vector (e.g.:for power = 5: [5 8 9 0 2]):\n');
            
        otherwise
            fprintf("\nERROR: TYPE %s IS UNDEFINED\n\n re-enter:\n\n",y.type);
            continue;
    end
    if nbp == 0
        y.xAxis = data.t_sub{1};
    else
        y.xAxis = data.t_sub{i};
    end
    y = updateSignal(y);

    plot(y.xAxis,y.signal)
    data.signals{i} = y;
    i = i+1;
end
final_signal.signal = [];
final_signal.xAxis = [];
for i = 1:length(data.signals)
    final_signal.signal = [final_signal.signal , data.signals{i}.signal];    
    final_signal.xAxis = [final_signal.xAxis, data.signals{i}.xAxis];
end
close Figure 1;
figure;
plot(final_signal.xAxis,final_signal.signal);
while(1)
 operation = input('Choose from the following operations to perform on this signal, enter any other letter to re-enter data of the signal:\na.amplitude scaling  b.time reversal   c.time shift    d.expanding the signal      e.compressing the signal     f.none\n','s');
    switch operation
        case 'a'
            value = input('Scale value = ');
            final_signal = scale(final_signal,value);
            
        case 'b'
            final_signal = timeReverse(final_signal);
            
        case 'c'
            value = input('shift value = ');
            final_signal = timeShift(final_signal,value);

        case 'd'
            value = input('expanding value = ');
            final_signal = expand(final_signal,value);
            
        case 'e'
            value = input('compressing value = ');
            final_signal = compress(final_signal,value);
            
        case 'f'
            break;

        otherwise
            fprintf('Invalid input.');
            continue;
    end
    plot(final_signal.xAxis,final_signal.signal);
end

plot(final_signal.xAxis,final_signal.signal);
title('Final form of the signal');

